/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
* @author : 石头哥哥
*         Project : LandlordsServer
*         Date: 13-7-27
*         Time: 上午9:48
*         Connect: 13638363871@163.com
*         packageName: PACKAGE_NAME
*
*         http://jodd.org/doc/http.html
*         springMVC+spring +mybatis
*/
public class testjodd {

    public static void main(String[] args) throws Exception {

//        HttpRequest httpRequest = HttpRequest.post("www.baidu.com");
//        httpRequest.form("mobile","13800138000","password","13800138000");
//        httpRequest.queryString();
//        httpRequest.send().body();
//        System.out.println(httpRequest.send().bodyText());
//        HttpRequest httpRequest = HttpRequest.get("http://168yy.cc/Files/Avatar/image1.gif");
//
//        jodd.http.HttpResponse response=httpRequest.send();
//
//        byte[] pic=response.body().getBytes();
//        System.out.println(pic);
//         saveToFile("http://168yy.cc/Files/Avatar/image1.gif");

//        String uploadImgPath=null;
//        String vitualPath= testjodd.class.getClassLoader().getResource("").getPath();
//        if (vitualPath!=null){
//            uploadImgPath=vitualPath+ "headImage/" ;  //保存玩家头像服务器路径
//        }
//        File fileUpload = new File(uploadImgPath);
//        if (!fileUpload.exists()){
//            fileUpload.mkdirs();
//        }
//        String real_path=  uploadImgPath+".gif";
//        FileOutputStream fos = new FileOutputStream(real_path);
//        fos.write(pic);
//        fos.close();

//        String task="0/90";
//        String[] token= task.split("/");
//        for (String str:token){
//             System.out.println(Integer.parseInt(str));
//        }

//        Vector<Integer> test=new Vector<Integer>();
//        test.add(1);
//        System.out.println(test.get(0));
//        System.out.println(test.get(0));
    }

    public static   void   saveToFile(String   destUrl)   {
        FileOutputStream   fos   =   null;
        BufferedInputStream bis   =   null;
        HttpURLConnection   httpUrl   =   null;
        URL   url   =   null;
        int   BUFFER_SIZE   =   1024;
        byte[]   buf   =   new   byte[BUFFER_SIZE];
        int   size   =   0;
        try   {
            url   =   new URL(destUrl);
            httpUrl   =   (HttpURLConnection)url.openConnection();
            httpUrl.connect();
            bis   =   new   BufferedInputStream(httpUrl.getInputStream());
            String uploadImgPath=testjodd.class.getClassLoader().getResource("").getPath()+"headImage/";
            File fileUpload = new File(uploadImgPath);
            if (!fileUpload.exists()){
                fileUpload.mkdirs();
            }
            String real_path=uploadImgPath+"account"+".gif";

            fos   =   new   FileOutputStream(real_path);

            while   ((size = bis.read(buf))!= -1)   {
                fos.write(buf,0,size);
            }
            fos.flush();
        }
        catch   (IOException e)   {

        }

        catch   (ClassCastException   e)   {

        }

        finally   {
            try   {
                fos.close();
                bis.close();
                httpUrl.disconnect();
            }

            catch   (IOException   e)   {

            }

            catch   (NullPointerException   e)   {

            }

        }

    }
}
