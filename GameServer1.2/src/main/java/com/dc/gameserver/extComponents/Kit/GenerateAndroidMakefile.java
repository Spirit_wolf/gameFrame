/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;

/**
 * @author 石头哥哥
 *         Date: 13-11-7</br>
 *         Time: 下午5:23</br>
 *         Package: mk</br>
 *         注解：  用于cocos2d-x引擎中android项目编译文件列表生成
 */
public class GenerateAndroidMakefile {
    /** 分隔符 */
    private static final String LINE_BREAK = System.getProperty("line.separator", "/n");
    /** classes文件夹 */
    private File classesDir = null;
    /** classes文件夹路径 */
    private String classesPath = "";
    /** classes文件夹相对路径 */
    private String classesRelativePath = "";
    /** 编译文件过滤器 */
    private BuildFileFilter buildFileFilter = null;

    /**
     * 创建GenerateAndroidMakefile
     * @param classesAbsolutePath classes文件夹绝对路径
     * @param classesRelativePath classes文件夹在编译文件中的相对路径
     * @throws Exception
     */
    public GenerateAndroidMakefile(String classesAbsolutePath, String classesRelativePath) throws Exception{
        if(classesRelativePath == null){
            throw new Exception("classes文件夹相对路径错误,不能为NULL!");
        }
        if(classesAbsolutePath == null || "".equals(classesAbsolutePath)){
            throw new Exception("classes文件夹路径输入错误,不能为空!");
        }
        classesDir = new File(classesAbsolutePath);
        if((!classesDir.exists()) || (!classesDir.canRead()) || (!classesDir.isDirectory())){
            throw new FileNotFoundException("classes文件夹不可读:"+classesDir.getAbsolutePath());
        }
        this.classesPath = classesAbsolutePath;
        this.classesPath = classesAbsolutePath.replaceAll("\\\\", "/");
        this.classesRelativePath = classesRelativePath;
        buildFileFilter = new BuildFileFilter();
    }

    /**
     * 输出编译文件列表
     */
    public void outputBuildFilesList(){
        StringBuilder buildFilesSb = new StringBuilder();
        outputBuildFileList(classesDir,buildFilesSb);
        System.out.println(buildFilesSb.toString());
    }
    private void outputBuildFileList(File buildfile,StringBuilder buildFilesSb){
        if(buildfile.isDirectory()){
            File[] files =buildfile.listFiles(buildFileFilter);
            for (File file : files) {
                outputBuildFileList(file,buildFilesSb);
            }
        }else{
            String buildfileStr = translateBuildFilePath(buildfile.getAbsolutePath()) + " \\";
            buildFilesSb.append(buildfileStr).append(LINE_BREAK);
        }
    }
    /**
     * 转换编译文件路径
     * @param filepath
     * @return
     */
    private String translateBuildFilePath(String filepath){
        return filepath.replaceAll("\\\\", "/").replace(classesPath, classesRelativePath);
    }

    /**
     * @param args
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws Exception {
        String classesPath = "D:\\developer\\cocos2d-x-2.1.4\\projects\\hszg_ol\\Classes";
        String relativePath = "                   ../../Classes";
        GenerateAndroidMakefile gam = new GenerateAndroidMakefile(classesPath,relativePath);
        gam.outputBuildFilesList();
    }


    /**
     * 编译文件过滤器
     * @author leeass
     *
     */
    class BuildFileFilter implements FileFilter {
        @Override
        public boolean accept(File pathname) {
            String name = pathname.getName().toLowerCase();
            return (!pathname.isHidden()) && (pathname.isDirectory() || name.endsWith(".c") || name.endsWith(".cpp"));
        }
    }
}
