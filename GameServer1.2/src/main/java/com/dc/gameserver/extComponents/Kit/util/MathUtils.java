/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.util;

/**
 * @auther:陈磊 <br/>
 * Date: 12-12-17<br/>
 * Time: 下午2:54<br/>
 * connectMethod:13638363871@163.com<br/>
 *
 */
public  class MathUtils {

    private static long negativeZeroDoubleBits = Double.doubleToLongBits(-0.0d);
    /**
     * 两点间的距离(x1,y1),(x2,y2)
     * @return  double
     */
    public static double  distance(double x1 ,double y1, double x2,  double y2){
        return  Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    /**
     *
     * 碰撞检测
     * 坐标 ：(x1,y1),(x2,y2)
     * @param r1 对象1 半径
     * @param r1  对象2 半径
     * @return boolean
     */
    public static boolean IsCollision(double x1 ,double y1, double x2,  double y2,double r1 ,double r2){
        if(distance(x1,y1,x2,y2)>(r1+r2)){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 计算正弦值
     * @param a
     * @return
     */
    public static double sin(double a) {
        return StrictMath.sin(a);
    }
    /**
     * 计算余弦值
     * @param a
     * @return
     */
    public static double cos(double a) {
        return StrictMath.cos(a);
    }

    public static double tan(double a) {
        return StrictMath.tan(a);
    }

    public static int max(int a, int b) {
        return (a >= b) ? a : b;
    }

    public static double max(double a, double b) {
        if (a != a) return a;   // a is NaN
        if ((a == 0.0d) && (b == 0.0d)
                && (Double.doubleToLongBits(a) == negativeZeroDoubleBits)) {
            return b;
        }
        return (a >= b) ? a : b;
    }
    public static int min(int a, int b) {
        return (a <= b) ? a : b;
    }

    public static double min(double a, double b) {
        if (a != a) return a;   // a is NaN
        if ((a == 0.0d) && (b == 0.0d)
                && (Double.doubleToLongBits(b) == negativeZeroDoubleBits)) {
            return b;
        }
        return (a <= b) ? a : b;
    }
}
