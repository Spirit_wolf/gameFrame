/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.SpringRabbitmq;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

/**
 * @author 石头哥哥</br>
 *         Date:14-3-8</br>
 *         Time:下午8:46</br>
 *         Package:com.dc.gameserver.extComponents.springRabbitmq</br>
 *         Comment：
 */
public class QueueOneLitener  implements MessageListener {
    @Override
    public void onMessage(Message message) {
        System.out.println(" data :" + message.getBody());
    }
}
