/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.util;
 

public class ClassUtil {
	/**
	 * 获取类的包路径
	 * 
	 * @param c
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getPackage(Class c) {
		String classPath = c.getName().substring(0,
				c.getName().lastIndexOf(".") + 1);
		return classPath;
	}

	public static void main(String[] args) {
		System.out.println(ClassUtil.getPackage(ClassUtil.class));
	}
	
	
}
