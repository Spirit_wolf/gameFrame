/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Scence;

public final class Location{
	private int _x;
	private int _y;
	private int _z;
	private int _heading;


	public Location(int x, int y, int z){
		_x = x;
		_y = y;
		_z = z;
	}

	public Location(int x, int y, int z, int heading){
		_x = x;
		_y = y;
		_z = z;
		_heading = heading;
	}

	public int getX(){
		return _x;
	}

	public int getY(){
		return _y;
	}

	public int getZ(){
		return _z;
	}

	public int getHeading(){
		return _heading;
	}
}
