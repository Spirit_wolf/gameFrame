/**

 @author 石头哥哥
 </P>
 Date:   2014/10/13
 </P>
 Time:   17:15
 </P>
 Package: gameFrame
 </P>

 注解：        逻辑处理

 */
package com.dc.gameserver.ServerCore.Controller.HandleController;

//
//import AbstractController;
//import PlayerInstance;
//import com.google.protobuf.MessageLite;
//import org.springframework.stereotype.Service;
//
//@Service    ---- spring 实例化
//public class TestController extends AbstractController {
//
//    /**
//     * 通过spring 来初始化controller的操作引用
//     * 单例--- 该方法会在spring容器初始化的时候 回调
//     */
//    @Override
//    public void PostConstruct() {
////        int ID = spriteLoadingRequest.msgID.ID_VALUE;
////        GAME_CONTROLLERS[ID] = this;
////        IController.MESSAGE_LITE[ID] = spriteLoadingRequest.getDefaultInstance();
//    }
//
//    /**
//     * messageLite数据结构体分发
//     *
//     * @param messageLite 数据载体
//     * @param player      active object
//     * @throws Exception 注意messageLite应该递交给GC直接处理  ，set null
//     */
//    @Override
//    public void DispatchMessageLit(MessageLite messageLite, PlayerInstance player) throws Exception {
//
//        // do logic      ,messageLite will be cast pb object
//    }
//}
