/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.cache.Memcached;

import net.rubyeye.xmemcached.GetsResponse;
import net.rubyeye.xmemcached.XMemcachedClient;
import net.rubyeye.xmemcached.exception.MemcachedException;

import java.util.concurrent.TimeoutException;

/**
 * @author 石头哥哥
 *         Date: 13-11-11</br>
 *         Time: 下午3:20</br>
 *         Package: Server.ExtComponents.memcache</br>
 *         注解：基于 memcached+spring+xmemcached的缓存封装
 *         应用场景：分布式环境
 */
//@Service
public class memcahcedService  implements ImemCache {


//    @Resource
    private XMemcachedClient memcachedClient;

    /**
     * set     key-----value
     *
     * @param key
     * @param value
     * @return
     * @throws TimeoutException
     *
     * @throws InterruptedException
     * @throws MemcachedException
     *                  exp default  value :0  never exp!
     */
    @Override
    public boolean set(String key, Object value) throws TimeoutException, InterruptedException, MemcachedException {
        return this.set(key,0,value);
    }

    /**
     * @param key
     * @param exp   单位 秒
     * @param value
     * @return
     * @throws TimeoutException
     *
     * @throws InterruptedException
     * @throws MemcachedException
     *
     */
    @Override
    public boolean set(String key, int exp, Object value) throws TimeoutException, InterruptedException, MemcachedException {
        return this.memcachedClient.set(key, exp, value);  //To change body of implemented methods use File | Settings | File Templates.
    }


    /**
     * 更新键-->值
     *
     * @param key
     * @return
     */
    @Override
    public boolean update(String key,Object value) throws InterruptedException, MemcachedException, TimeoutException {
        boolean status=true;
        GetsResponse <Object> result = this.memcachedClient.gets(key);
        long cas=result.getCas();
        if (!this.memcachedClient.cas(key,0,value,cas)) {
            status=false;
        }
         return status;
    }


    /**
     * @param key
     * @param value
     * @return
     * @throws TimeoutException
     *
     * @throws InterruptedException
     * @throws MemcachedException
     *
     */
    @Override
    public boolean replace(String key, Object value) throws TimeoutException, InterruptedException, MemcachedException {
        return this.replace(key, 0,value);  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * @param key
     * @param exp
     * @param value
     * @return
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws MemcachedException
     *
     */
    @Override
    public boolean replace(String key, int exp, Object value) throws TimeoutException, InterruptedException, MemcachedException {
        return  this.memcachedClient.set(key, exp, value);  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * @param key
     * @param <T>
     * @return
     * @throws TimeoutException
     *
     * @throws InterruptedException
     * @throws MemcachedException
     *
     */
    @Override
    public <T> T get(String key) throws TimeoutException, InterruptedException, MemcachedException {
        return this.memcachedClient.get(key);  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Delete key's date item from memcached
     *
     * @param key Operation timeout
     * @return
     * @throws TimeoutException
     *
     * @throws InterruptedException
     * @throws MemcachedException
     *
     * @since 1.3.2
     */
    @Override
    public boolean delete(String key) throws TimeoutException, InterruptedException, MemcachedException {
        return this.delete(key,0);  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Delete key's date item from memcached
     *
     * @param key
     * @param opTimeout Operation timeout
     * @return
     * @throws TimeoutException
     *
     * @throws InterruptedException
     * @throws MemcachedException
     *
     * @since 1.3.2
     */
    @Override
    public boolean delete(String key, long opTimeout) throws TimeoutException, InterruptedException, MemcachedException {
        return this.memcachedClient.delete(key, opTimeout);  //To change body of implemented methods use File | Settings | File Templates.
    }
}
